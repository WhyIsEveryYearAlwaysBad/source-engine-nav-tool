# Program information.
## Program name.
PROGRAM_NAME=source-engine-nav-tool
## Program version.
PROGRAM_VERSION=0.0.1

# Generic build options.
CXX=g++-10
CXX_FLAGS=-std=c++2a -g -Wall
OBJ_DIR=obj
BUILD_DIRECTORY=builddir
SRC_DIR=src
OBJECTS=utils.o test_automation.o nav_connections.o nav_base.o nav_area.o nav_file.o nav_tool.o

# Man page build options
MAN_PAGE_DIRECTORY=docs/man-pages

# "You're a phony. A big fat phony!" - Some random dude from family guy.
.PHONY: prep build install uninstall clean man-pages help

# Build the program.
build: prep $(OBJECTS) $(PROGRAM_NAME)

utils.o: $(SRC_DIR)/utils.cpp $(SRC_DIR)/utils.hpp
	$(CXX) -c $(CXX_FLAGS) $^

test_automation.o: $(SRC_DIR)/test_automation.cpp $(SRC_DIR)/test_automation.hpp
	$(CXX) -c $(CXX_FLAGS) $^

nav_tool.o: $(SRC_DIR)/nav_tool.hpp $(SRC_DIR)/nav_tool.cpp
	$(CXX) -D "PROGRAM_VERSION=\"$(PROGRAM_VERSION)\"" -c $(CXX_FLAGS) $^

nav_base.o: $(SRC_DIR)/nav_base.cpp $(SRC_DIR)/nav_base.hpp $(SRC_DIR)/nav_place.cpp $(SRC_DIR)/nav_place.hpp
	$(CXX) -c $(CXX_FLAGS) $^

nav_connections.o: $(SRC_DIR)/nav_connections.cpp $(SRC_DIR)/nav_connections.hpp
	$(CXX) -c $(CXX_FLAGS) $^

nav_area.o: $(SRC_DIR)/nav_area.cpp $(SRC_DIR)/nav_area.hpp
	$(CXX) -c $(CXX_FLAGS) $^

nav_file.o: $(SRC_DIR)/nav_file.cpp $(SRC_DIR)/nav_file.hpp
	$(CXX) -c $(CXX_FLAGS) $^

$(PROGRAM_NAME): $(OBJECTS)
	$(CXX) $(CXX_FLAGS) -o $(BUILD_DIRECTORY)/$@ $^

# Prepares for building.
prep:
	mkdir -p $(BUILD_DIRECTORY)/$(OBJ_DIR)
	cd $(BUILD_DIRECTORY)

# Installs program to /usr/local.
install:
	sudo cp $(BUILD_DIRECTORY)/$(PROGRAM_NAME) /usr/local/bin
	sudo cp -f $(BUILD_DIRECTORY)/man1/* /usr/local/share/man/man1/

# Uninstalls program.
uninstall:
	sudo rm -f "/usr/local/bin/$(PROGRAM_NAME)"
	cd /usr/local/share/man/man1/
	sudo rm -f "$(PROGRAM_NAME)*.man1"

# Removes leftover files from compilation.
clean:
	rm -rf $(BUILD_DIRECTORY)
	find . -name "*.gch" -exec rm {} \;
	find . -name "*.o" -exec rm {} \;

# Build man pages.
man-pages:
	mkdir -p $(BUILD_DIRECTORY)/man1
	cp -f $(MAN_PAGE_DIRECTORY)/* $(BUILD_DIRECTORY)/man1
	for Page in $$(find $(BUILD_DIRECTORY) -type f | grep ".1$$"); do sed -i 's|$$(date)|'"$$(date +%F)|" "$$Page"; done

# Display help text.
help:
	printf "makefile arguments:\n\tbuild - Compiles the program.\n\tinstall - Installs program and man pages to /usr/local.\n\tuninstall - Uninstalls program and man pages.\n\thelp - Displays this text.\n\tman-pages - Build program man pages."
