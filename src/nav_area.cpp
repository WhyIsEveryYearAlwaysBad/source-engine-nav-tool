#include <iostream>
#include <iomanip>
#include <fstream>
#include <climits>
#include <memory>
#include "utils.hpp"
#include "nav_area.hpp"
#include "nav_base.hpp"


NavArea::~NavArea() {
	
}

// Outputs navigation area data in an awk-friendly key-value format.
void NavArea::OutputData(std::ostream& ostream) const {
  // Output area id.
  ostream << "area_id: " << ID
	  << "\nattribute_flag: " << Flags
    // Output north west corner position.
	  << "\nnorth_west_corner: ";
  // Output extended corner heights.
	std::copy(nwCorner.begin(), nwCorner.end(), std::ostream_iterator<float>(ostream, " "));
	ostream << "\nsouth_east_corner: ";
	std::copy(seCorner.begin(), seCorner.end(), std::ostream_iterator<float>(ostream, " "));
	ostream << "\nnorth_east_corner_height: " << NorthEastZ
	<< "\nsouth_west_corner_height: " << SouthWestZ
	  // Output connection counts for each direction.
	<< "\nconnection_counts: ";
	std::transform(connectionData.begin(), connectionData.end(), std::ostream_iterator<unsigned int>(ostream, " "), [](auto& connectionSequence) { return connectionSequence.first; });
	// Output comment for clarity.
	ostream << "# In NESW Order.";
	ostream << "\nhiding_spot_count: " << static_cast<unsigned int>(hideSpotSeq.first);
	if (approachSpotSequence.has_value()) ostream << "\napproach_spot_count: " << approachSpotSequence.value().first;
	if (ladderSeq.has_value()) {
	  ostream << "\nladder_count: ";
	  std::transform(ladderSeq.value().cbegin(), ladderSeq.value().cend(), std::ostream_iterator<unsigned int>(ostream, " "), [](auto& seqPair) { return seqPair.first; } );
	}
	ostream << "\nencounter_path_count: " << encounterPathSequence.first
	<< "\nplace_id: " << PlaceID;
	ostream << "\nearliest_travel_times: ";
	std::copy(EarliestOccupationTimes.begin(), EarliestOccupationTimes.end(), std::ostream_iterator<float>(ostream, " "));
	// Output light intensity values.
	if (LightIntensity.has_value())
	{
		ostream << "\nlight_intensity: ";
		std::copy(LightIntensity.value().begin(), LightIntensity.value().end(), std::ostream_iterator<float>(ostream, " "));
	}
	ostream << "# Values are ordered in NESW coordinates."; // Output direction order for clarity.
	// Output area-bind count IF it actually exists.
	if (areaBindSequence.has_value()) {
		ostream << "\narea_bind_count: " << areaBindSequence.value().first;
	}
	ostream << "\nvisibility_inheritor_area_id: " << InheritVisibilityFromAreaID << '\n';
}



// Fill data from buffer.
// Returns true on success, false on failure.
bool NavArea::ReadData(std::streambuf& buf, const unsigned int& NAVVersion, const std::optional<unsigned int>& SubVersion) {
	std::streampos startPos = buf.pubseekoff(0, std::ios_base::cur);
	if (buf.sgetn(reinterpret_cast<char*>(&ID), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		std::clog << "NavArea::ReadData(): Could not read area ID!\n";
		return false;
	}; // ID
	{
		// 8-bit flag
		if (NAVVersion < 8) {
			Flags = buf.sbumpc();
			if (Flags == EOF) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read area 8-bit flag!\n", "");
				return false;
			}
		}
		// 16-bit flag
		else if (NAVVersion <= 13) {
			unsigned short tmp;
			if (buf.sgetn(reinterpret_cast<char*>(&tmp), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read area 16-bit flag!\n", "");
				return false;
			}
			Flags = tmp;
		}
		// 32-bit flag
		else {
			if (buf.sgetn(reinterpret_cast<char*>(&Flags), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read area 32-bit flag!\n", "");
				return false;
			}
		}
	}
	for (size_t i = 0; i < nwCorner.size(); i++)
	{
		if (buf.sgetn(reinterpret_cast<char*>(nwCorner.data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
			ERROR_MESSAGE("NavArea::ReadData(): Could not read nwCorner["+std::to_string(i)+"]!\n", "");
			return false;
		}
	}
	for (size_t i = 0; i < seCorner.size(); i++)
	{
		if (buf.sgetn(reinterpret_cast<char*>(seCorner.data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
			ERROR_MESSAGE("NavArea::ReadData(): Could not read nwCorner["+std::to_string(i)+"]!\n", "");
			return false;
		}
	}
	if (buf.sgetn(reinterpret_cast<char*>(&NorthEastZ), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
		std::clog << "NavArea::ReadData(): Could not get NorthEastZ!\n";
		return false;
	}
	if (buf.sgetn(reinterpret_cast<char*>(&SouthWestZ), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
		std::clog << "NavArea::ReadData(): Could not get SouthWestZ!\n";
		return false;
	}
	for (unsigned char currDirection = static_cast<unsigned char>(Direction::North); currDirection < static_cast<unsigned char>(Direction::Count); currDirection++)
	{
		if (buf.sgetn(reinterpret_cast<char*>(&connectionData[currDirection].first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			ERROR_MESSAGE("NavArea::ReadData(): Could not get direction " << static_cast<unsigned char>(currDirection) <<" connection data count!\n", "");
			return false;
		}
		connectionData[currDirection].second.resize(connectionData[currDirection].first);
		for (size_t i = 0; i < connectionData[currDirection].first; i++)
		{
			if (!connectionData[currDirection].second.at(i).ReadData(buf)) {
				ERROR_MESSAGE("NavArea::ReadData(): Failed to read connection data for direction "+std::to_string(currDirection)+"!\n", "");
				return false;
			}
		}
	}
	// Hiding spots (were added in NAV version 1).
	if (NAVVersion >= 1) {
		if (buf.sgetn(reinterpret_cast<char*>(&hideSpotSeq.first), VALVE_CHAR_SIZE) != VALVE_CHAR_SIZE) {
			ERROR_MESSAGE("NavArea::ReadData(): Could not get hide spot count!\n", "");
			return false;
		}
		for (size_t i = 0; i < hideSpotSeq.first; i++)
		{
			hideSpotSeq.second.emplace_back();
			if (!hideSpotSeq.second.back().ReadData(buf, NAVVersion)) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read hide spot data!\n", "");
				return false;
			}
		}
	}

	// Approach spots were removed in NAV Version 15, so only read if NAV version reports older than that.
	if (NAVVersion < 15) {
		approachSpotSequence.emplace();
		approachSpotSequence.value().first = buf.sbumpc();
		if (approachSpotSequence.value().first == std::streambuf::traits_type::eof()) {
			ERROR_MESSAGE("NavArea::ReadData(): Could not read approach spot count!\n", "");
			return false;
		}
		for (size_t i = 0; i < approachSpotSequence.value().first; i++)
		{
			approachSpotSequence.value().second.emplace_back();
			if (!approachSpotSequence.value().second.back().ReadData(buf)) {
				ERROR_MESSAGE("NavArea::ReadData(): Failed to read approach spot data!\n", "");
				return false;
			}
		}
	}

	// Encounter Paths.
	if (buf.sgetn(reinterpret_cast<char*>(&encounterPathSequence.first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		ERROR_MESSAGE("NavArea::ReadData(): Could not get encounter path count!\n", "");
		return false;
	}
	encounterPathSequence.second.resize(encounterPathSequence.first);
	for (unsigned int pathIndex=0; pathIndex < encounterPathSequence.first; pathIndex++) {
		if (!encounterPathSequence.second.at(pathIndex).ReadData(buf)) {
			ERROR_MESSAGE("NavArea::ReadData(): Could not read encounter path data!\n", "");
			return false;
		}
	}
	// Read place ID.
	if (buf.sgetn(reinterpret_cast<char*>(&PlaceID), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
		ERROR_MESSAGE("NavArea::ReadData(): Could not get place ID!\n", "");
		return false;
	}
	// Ladder IDs
	if (NAVVersion >= 7) {
	  ladderSeq.emplace();
	  for (unsigned char ladderDirection = 0; ladderDirection < ladderSeq.value().size(); ladderDirection++)
	    {
	      if (buf.sgetn(reinterpret_cast<char*>(&ladderSeq.value().at(ladderDirection).first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		ERROR_MESSAGE("NavArea::ReadData(): Could not get ladder data count for index "+std::to_string(ladderDirection)+"!\n", "");
		return false;
	      }
	      for (size_t it = 0; it < ladderSeq.value().at(ladderDirection).first; it++)
		{
		  IntID ladderID;
		  if (buf.sgetn(reinterpret_cast<char*>(&ladderID), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		    ERROR_MESSAGE("NavArea::ReadData(): Could not read ladder ID!\n", "");
		    return false;
		  }
		  ladderSeq.value().at(ladderDirection).second.push_back(ladderID);
		}
	    }
	}
	// Occupy times
	if (NAVVersion >= 8) {
		for (size_t i = 0; i < EarliestOccupationTimes.size(); i++)
		{
			if (buf.sgetn(reinterpret_cast<char*>(EarliestOccupationTimes.data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read occupy time to index "+std::to_string(i)+"!\n", "");
				return false;
			}
		}
	}
	// Light intensity
	if (NAVVersion >= 11) {
		LightIntensity.emplace(std::array<float, 4>({0.0f, 0.0f, 0.0f, 0.0f}));
		for (size_t i = 0; i < static_cast<unsigned char>(Direction::Count); i++)
		{
			if (buf.sgetn(reinterpret_cast<char*>(LightIntensity.value().data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read light intensity for corner["+std::to_string(i)+"]!\n", "");
				return false;
			}
		}
	}
	// Visible areas
	if (NAVVersion >= 16) {
		areaBindSequence.emplace();
		{
			if (buf.sgetn(reinterpret_cast<char*>(&areaBindSequence.value().first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read visible area count!\n", "");
				return false;
			}
		}
		areaBindSequence.value().second.resize(areaBindSequence.value().first);
		for (size_t i = 0; i < areaBindSequence.value().first; i++)
		{
			if (!areaBindSequence.value().second.at(i).ReadData(buf)) {
				ERROR_MESSAGE("NavArea::ReadData(): Could not read visArea data!\n", "");
				return false;
			}
		}
		// Get InheritVisibilityFromAreaID.
		if (buf.sgetn(reinterpret_cast<char*>(&InheritVisibilityFromAreaID), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			std::clog << "NavArea::ReadData(): Could not read InheritVisibilityFromAreaID!\n";
			return false;
		}
	}
	// Create custom game data.
	std::streampos customDataPos = buf.pubseekoff(0, std::ios_base::cur, std::ios_base::in);
	customDataSize = getAreaCustomDataSize(buf, NAVVersion, SubVersion);
	buf.pubseekpos(customDataPos, std::ios_base::in);
	// Make room for custom data.
	customData.clear();
	customData.resize(customDataSize);
	// Read custom data.
	if (buf.sgetn(reinterpret_cast<char*>(customData.data()), customDataSize) != customDataSize) {
		ERROR_MESSAGE("NavArea::ReadData(): Could not read custom data!\n", "");
		return false;
	}
	// Set area size
	size = buf.pubseekoff(0, std::ios_base::cur) - startPos;
	// Done.
	return true;
}

// Write the nav data into a stream.
bool NavArea::WriteData(std::streambuf& out, const unsigned int& Version, const std::optional<unsigned int>& SubVersion) {
	if (out.sputn(reinterpret_cast<char*>(&ID), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		ERROR_MESSAGE("NavArea::WriteData(): Could not write area ID!\n", "");
		return false;
	}
	if (Version < 8) {
		unsigned char tmp = Flags;
		if (out.sputc(tmp) == std::streambuf::traits_type::eof()) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write 8-bit attribute flag!\n", "");
			return false;
		}
	}
	else if (Version <= 13) {
		unsigned short tmp = Flags;
		if (out.sputn(reinterpret_cast<char*>(&tmp), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write 16-bit attribute flag!\n", "");
			return false;
		}
	}
	else {
		if (out.sputn(reinterpret_cast<char*>(&Flags), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write 32-bit attribute flag!\n", "");
			return false;
		}
	}

	// Write nwCorner corner vector.
	for (size_t i = 0; i < nwCorner.size(); i++)
	{
		if (out.sputn(reinterpret_cast<char*>(nwCorner.data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write nwCorner["+std::to_string(i)+"]!\n", "");
			return false;
		}
	}
	// Write seCorner corner vector.
	for (size_t i = 0; i < seCorner.size(); i++)
	{
		if (out.sputn(reinterpret_cast<char*>(seCorner.data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write seCorner["+std::to_string(i)+"]!\n", "");
			return false;
		}
	}
	// Write out North-East and South-West corner heights.
	if (out.sputn(reinterpret_cast<char*>(&NorthEastZ), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavArea::WriteData(): Failed to write North-East corner height!\n";
		#endif
		return false;
	}
	if (out.sputn(reinterpret_cast<char*>(&SouthWestZ), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavArea::WriteData(): Failed to write South-West corner height!\n";
		#endif
		return false;
	}
	// Write connection data.
	for (unsigned char currDirection = static_cast<unsigned char>(Direction::North); currDirection < static_cast<unsigned char>(Direction::Count); currDirection++)
	{
		if (out.sputn(reinterpret_cast<char*>(&connectionData[currDirection].first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			#ifndef NDEBUG
			std::clog << "NavArea::WriteData(): Failed to write conneciton count for direction "<<currDirection<<"!\n";
			#endif
			return false;
		}
		for (size_t i = 0; i < connectionData[currDirection].first; i++)
		{
			if (!connectionData[currDirection].second[i].WriteData(out)) {
				#ifndef NDEBUG
				std::clog << "NavArea::WriteData(): Failed to write connection data!\n";
				#endif
				return false;
			}
		}
	}
	// Hiding spots (were added in version 1)
	if (Version >= 1) {
		if (out.sputc(hideSpotSeq.first) == std::streambuf::traits_type::eof()) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write hide spot count!\n", "");
			return false;
		}
		for (size_t i = 0; i < hideSpotSeq.first; i++)
		{
			if (!hideSpotSeq.second.at(i).WriteData(out, Version)) {
				ERROR_MESSAGE("NavArea::WriteData(): Failed to write hide spot data!\n", "");
				return false;
			}
		}
	}
	// Approach spots were removd in NAV version 15.
	if (Version < 15) {
		if (!approachSpotSequence.has_value()) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write approach spot sequence. (Sequence does not exist!)\n", "");
			return false;
		}
		if (out.sputc(approachSpotSequence.value().first) == EOF) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write approach spot count!\n", "");
			return false;
		}
		for (size_t i = 0; i < approachSpotSequence.value().first; i++)
		{
			if (!approachSpotSequence.value().second.at(i).WriteData(out)) {
				ERROR_MESSAGE("NavArea::WriteData(): Failed to write approach spot data!\n", "");
				return false;
			}
		}
	}
	// Encounter Paths.
	if (out.sputn(reinterpret_cast<char*>(&encounterPathSequence.first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		ERROR_MESSAGE("NavArea::WriteData(): Failed to write encounter path count!\n", "");
		return false;
	}
	for (size_t i = 0; i < encounterPathSequence.first; i++)
	{
		if (!encounterPathSequence.second.at(i).WriteData(out)) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write encounter path data!\n", "");
			return false;
		}
	}
	// Write place ID.
	if (out.sputn(reinterpret_cast<char*>(&PlaceID), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavArea::WriteData(): Failed to write place ID!\n";
		#endif
		return false;
	}
	// Handle ladders
	if (Version >= 7) {
	  if (!ladderSeq.has_value()) {
	    ERROR_MESSAGE("NavArea::WriteData(): Ladder sequence is undefined!\n", "");
	    return false;
	  }
	  for (unsigned char ladderDirection = 0; ladderDirection < ladderSeq.value().size(); ladderDirection++)
	    {
	      // Ladder count
	      if (out.sputn(reinterpret_cast<char*>(&ladderSeq.value().at(ladderDirection).first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
#ifndef NDEBUG
		std::clog << "NavArea::WriteData(): Failed to write (direction "<<ladderDirection<<") ladder count!\n";
#endif
		return false;
	      }
	      // Data
	      for (size_t it = 0u; it < ladderSeq.value().at(ladderDirection).first; it++)
		{
		  if (out.sputn(reinterpret_cast<char*>(&ladderSeq.value().at(ladderDirection).second.at(it)), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		    ERROR_MESSAGE("NavArea::WriteData(): Failed to write (direction "+std::to_string(ladderDirection)+") ladder data!\n", "");
		    return false;
		  }
		}
	    }
	}
	// Occupy times (were added in version 8).
	if (Version >= 8)
	{
		for (size_t i = 0u; i < EarliestOccupationTimes.size(); i++) {
			if (out.sputn(reinterpret_cast<char*>(EarliestOccupationTimes.data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
				ERROR_MESSAGE("NavArea::WriteData(): Failed to write occupation time for team "+std::to_string(i)+" data!\n", "");
				return false;
			}
		}
	}
	// Light intensity
	if (Version >= 11) {
		if (!LightIntensity.has_value()) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write light intensity data. (LightIntensity does not exist!)\n", "");
			return false;
		}
		for (size_t i = 0u; i < LightIntensity.value().size(); i++) {
			if (out.sputn(reinterpret_cast<char*>(LightIntensity.value().data() + i), VALVE_FLOAT_SIZE) != VALVE_FLOAT_SIZE) {
				ERROR_MESSAGE("NavArea::WriteData(): Failed to write LightIntensity. (Wrote mismatching bytes!)\n", "");
				return false;
			}
		}
	}

	// area-bind data.
	if (Version >= 16) {
		if (!areaBindSequence.has_value()) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write area bind sequence, because it is not defined.\n", "");
			return false;
		}
		// Write area bind count.
		if (out.sputn(reinterpret_cast<char*>(&areaBindSequence.value().first), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write visibility area count!\n", "");
			return false;
		}
		// Write out area binds.
		for (size_t index = 0u; index < areaBindSequence.value().first; index++)
		{
			if (!areaBindSequence.value().second.at(index).WriteData(out)) {
				ERROR_MESSAGE("NavArea::WriteData(): Failed to write area bind data.\n", "");
				return false;
			}
		}
		// Try to write InheritVisibilityFromAreaID.
		if (out.sputn(reinterpret_cast<char*>(&InheritVisibilityFromAreaID), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			ERROR_MESSAGE("NavArea::WriteData(): Failed to write InheritVisibilityFromAreaID!\n", "");
			return false;
		}
	};
	// Write custom data.
	if (out.sputn(reinterpret_cast<char*>(customData.data()), customDataSize) != customDataSize) {
		std::clog << "NavArea::WriteData(): Failed to write custom data!\n";
		return false;
	}
	return true;
}
