#ifndef NAV_AREA_HPP
#define NAV_AREA_HPP
#include <deque>
#include <list>
#include <variant>
#include <utility>
#include <memory>
#include <vector>
#include <any>
#include "nav_base.hpp"
#include "nav_connections.hpp"
#include "nav_place.hpp"

class NavArea {
	public:

	~NavArea();
	// Total size of the Navigation Area (in bytes).
	size_t size = 0u;
	/* Nav Area data */
	IntID ID = 0u;// ID of the NavArea.
	unsigned int Flags; // Attributes set on this area.
	std::array<float, 3> nwCorner; // Location of the north-west corners.
	std::array<float, 3> seCorner; // Location of the south-east corners.
	// The height of the north-east and south-west corners.
	float NorthEastZ, SouthWestZ;
	/* 
		Sequence of light intensity values of all four corners in the order of North, East, South, and West.
		Added in NAV version 11.
	*/
	std::optional<std::array<float, 4> > LightIntensity;
	// Amount of connections and the connection data in each direction (NESW).
	std::array<std::pair<unsigned int, std::vector<NavConnection> >, 4> connectionData = {
		std::make_pair(0, std::vector<NavConnection>()),
		std::make_pair(0, std::vector<NavConnection>()),
		std::make_pair(0, std::vector<NavConnection>()),
		std::make_pair(0, std::vector<NavConnection>())};
		
	unsigned short PlaceID = 0; // The ID of the place this area is in.

	/* Sequence of hide spots.
		first - Amount of hide spots.
		second - Container for hide spot objects.
		Hide Spots were added in NAV Version 1.
	*/
	std::pair<unsigned char, std::vector<NavHideSpot> > hideSpotSeq;
	/*
		first - Amount of approach spots.
		second - Container for approach spot objects.
		Approach Spots were removed in NAV Version 15.
	*/
	std::optional<std::pair<unsigned char, std::deque<NavApproachSpot> > > approachSpotSequence;
	/*
		@brief Container for ladder data.
		Stores 2 (Directions) of pairs that store the ladder count and ladder data respectively.
		
		Added in NAV Version 7.
	*/
  std::optional<std::array<std::pair<unsigned int, std::deque<IntID> >, 2> > ladderSeq;
	// Sequence of encounter paths.
	// first - Amount of encounter paths.
	// second - Dequeue container for NavEncounterPath objects. Dequeues are used here, because encounter paths often burgeon in size during analysis.
	std::pair<unsigned int, std::deque<NavEncounterPath> > encounterPathSequence;
	std::array<float, 2> EarliestOccupationTimes = {0.0f, 0.0f}; // The earliest time teams can occupy this area
// Burgeonative container for area binds.
	std::optional<std::pair<unsigned int, std::deque<NavAreaBind> > > areaBindSequence;
	unsigned int InheritVisibilityFromAreaID = 0u; // ID of the area to inherit our visibility from*/

	// Game-specific datum count
	size_t customDataSize = 0u;
	// Game-specific data.
	std::vector<unsigned char> customData;
	// Funcs
	void OutputData(std::ostream& ostream) const;

	// Write data to stream.
	// Returns true if successful, false on failure.
	bool WriteData(std::streambuf& out, const unsigned int& Version, const std::optional<unsigned int>& SubVersion);

	// Fills data from stream.
	bool ReadData(std::streambuf& buf, const unsigned int& NAVVersion, const std::optional<unsigned int>& SubVersion);
};
#endif
