#include <fstream>
#include <string>
#include <iostream>
#include <exception>
#include <bit>
#include <unistd.h>
#include <functional>
#include "nav_file.hpp"
#include "nav_area.hpp"

/* Write NAV data to stream.
Returns true on success, false on failure. */
bool NavFile::WriteData(std::streambuf& buf) {
	// Write header.
	if (buf.sputn(reinterpret_cast<char*>(&MagicNumber), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::WriteData(): failed to write magic number.\n";
		#endif
		return false;
	}
	// Write major version.
	if (buf.sputn(reinterpret_cast<char*>(&Version), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::WriteData(): failed to write major version.\n";
		#endif
		return false;
	}
	// Write minor version if Version is at least 10.
	{
		unsigned int tmp = SubVersion.value_or(0u);
		if (Version >= 10 && buf.sputn(reinterpret_cast<char*>(&tmp), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			#ifndef NDEBUG
			std::clog << "NavFile::WriteData(): failed to write minor version.\n";
			#endif
			return false;
		}

		// Write BSPSize if major version is ≥4.
		tmp = BSPSize.value_or(0u);
		if (Version >= 4 && buf.sputn(reinterpret_cast<char*>(&tmp), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
			#ifndef NDEBUG
			std::clog << "NavFile::WriteData(): failed to write BSP size.\n";
			#endif
			return false;
		}
	}
	// Write analyzed boolean if major version is ≥14.
	{
		bool tmp = isAnalyzed.value_or(false);
		if (Version >= 14 && buf.sputc(tmp) == EOF) {
			#ifndef NDEBUG
			std::clog << "NavFile::WriteData(): failed to write analyzed boolean.\n";
			#endif
			return false;
		}
	}

	// Write place count.
	if (Version >= 5)  {
		if (buf.sputn(reinterpret_cast<char*>(&PlaceCount), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
			#ifndef NDEBUG
			std::clog << "NavFile::WriteData(): failed to write place count.\n";
			#endif
			return false;
		}
		{
			// Write place data.
			if (PlaceNames.size() < PlaceCount) PlaceNames.resize(PlaceCount);
			if (!std::all_of(PlaceNames.begin(), PlaceNames.end(), [&buf](std::string& placeName) -> bool {
				unsigned short placeNameLength = std::clamp<unsigned short>(placeName.length(), 0u, UINT16_MAX);
				if (buf.sputn(reinterpret_cast<char*>(&placeNameLength), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
					#ifndef NDEBUG
					std::clog << "NavFile::WriteData(): failed to write place name length.\n";
					#endif
					return false;
				}
				if (buf.sputn(placeName.data(), placeNameLength) != placeNameLength) {
					#ifndef NDEBUG
					std::clog << "NavFile::WriteData(): failed to write place name.\n";
					#endif
					return false;
				}
				return true;
			})) {
				#ifndef NDEBUG
				std::clog << "NavFile::WriteData(): failed to write place data.\n";
				#endif
				return false;
			}
		}

		// Has unnamed areas?
		if (Version > 11 && buf.sputc(hasUnnamedAreas.value_or(false)) == EOF) {
			#ifndef NDEBUG
			std::clog << "NavFile::WriteData(): Could not write 'unnamed areas exist' boolean.\n";
			#endif
			return false;
		}
	}
	
	// Write area count.
	if (buf.sputn(reinterpret_cast<char*>(&AreaCount), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::WriteData(): Could not write area count.\n";
		#endif
		return false;
	}
	AreaDataLoc = buf.pubseekoff(0, std::ios_base::cur);
	// Writes area data.
	if (AreaCount > 0u) {
		unsigned int index = 0u;
		if (!std::all_of(areas.begin(), areas.end(), [&buf, &majV = Version, &minV = SubVersion, &index](NavArea& area) -> bool {
			if (area.WriteData(buf, majV, minV)) {
				index++;
				return true;
			}
			else {
				#ifndef NDEBUG
				std::clog << "NavFile::WriteData(): Failed to write area data.\n";
				#endif
				return false;
			}
		})) 
			return false;
	}
	
	// Write ladder count
	if (buf.sputn(reinterpret_cast<char*>(&LadderCount), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::WriteData(): Could not write area count.\n";
		#endif
		return false;
	}
	LadderDataLoc = buf.pubseekoff(0, std::ios_base::cur);
	for (auto& ladder : ladders)
	{
		if (!ladder.WriteData(buf))
		{
			#ifndef NDEBUG
			std::clog << "NavFile::WriteData(): Could not write ladder data.\n";
			#endif
			return false;
		}
	}
	return true;
}
/* Read header info.
Returns true on success, false on failure. */
bool NavFile::ReadData(std::streambuf& buf) {
	// Read header.
	if (buf.sgetn(reinterpret_cast<char*>(&MagicNumber), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::ReadData(): failed to write magic number.\n";
		#endif
		return false;
	}
	// Read major version.
	if (buf.sgetn(reinterpret_cast<char*>(&Version), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::ReadData(): failed to write major version.\n";
		#endif
		return false;
	}
	// Read minor version if Version is at least 10.
	{
		unsigned int tmp;
		if (Version >= 10) {
			if (buf.sgetn(reinterpret_cast<char*>(&tmp), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
				#ifndef NDEBUG
				std::clog << "NavFile::ReadData(): failed to write minor version.\n";
				#endif
				return false;
			}
			SubVersion = tmp;
		}

		// Read BSPSize if major version is ≥4.
		if (Version >= 4) {
			if (buf.sgetn(reinterpret_cast<char*>(&tmp), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
				#ifndef NDEBUG
				std::clog << "NavFile::ReadData(): failed to write BSP size.\n";
				#endif
				return false;
			}
			BSPSize = tmp;
		}
	}
	// Read analyzed boolean if major version is ≥14.
	{
		if (Version >= 14 && (isAnalyzed = buf.sbumpc()) == EOF) {
			#ifndef NDEBUG
			std::clog << "NavFile::ReadData(): failed to read analyzed boolean.\n";
			#endif
			return false;
		}
	}

	// Read place count.
	if (Version >= 5)  {
		if (buf.sgetn(reinterpret_cast<char*>(&PlaceCount), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
			#ifndef NDEBUG
			std::clog << "NavFile::ReadData(): failed to read place count.\n";
			#endif
			return false;
		}

		// Prepare place deque.
		PlaceNames.resize(PlaceCount);
		// Read place data.
		for (unsigned int i = 0u; i < PlaceCount; i++) {
			unsigned short placeNameLength;
			if (buf.sgetn(reinterpret_cast<char*>(&placeNameLength), VALVE_SHORT_SIZE) != VALVE_SHORT_SIZE) {
				#ifndef NDEBUG
				std::clog << "NavFile::ReadData(): failed to read place name length.\n";
				#endif
				return false;
			}
			//std::vector<char> placeNameBuf[256];
			PlaceNames.at(i).resize(placeNameLength);
			if (buf.sgetn(PlaceNames.at(i).data(), placeNameLength) != placeNameLength) {
				#ifndef NDEBUG
				std::clog << "NavFile::ReadData(): failed to read place name.\n";
				#endif
				return false;
			}
		}

		// Read has unnamed areas?
		if (Version > 11) {
			hasUnnamedAreas = buf.sbumpc();
			if (hasUnnamedAreas == EOF) {
				#ifndef NDEBUG
				std::clog << "NavFile::ReadData(): Could not read 'unnamed areas exist' boolean.\n";
				#endif
				return false;
			}
		}
	}
	/*
		NOTE: Left 4 Dead and some games load custom data here.
		Not really sure how to implement the reading though, so just assume that there's no custom data here for now.
	*/
	// Read area count.
	if (buf.sgetn(reinterpret_cast<char*>(&AreaCount), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::ReadData(): Could not read area count.\n";
		#endif
		return false;
	}

	AreaDataLoc = buf.pubseekoff(0, std::ios_base::cur);
	// Reserve memory for areas.
	if (!(AreaCount > 0)) areas = std::vector<NavArea>(AreaCount);
	else {
		areas.clear();
		areas.resize(AreaCount);
	}
	// Store area data.
	unsigned int index = 0u;
	if (!std::all_of(areas.begin(), areas.end(), [&buf, &majV = Version, &minV = SubVersion, &fileCache = cache, &index](NavArea& area) mutable {
		if (area.ReadData(buf, majV, minV)) {
			// Store highest and lowest area ID range.
			if (area.ID < fileCache.AreaIDRange.first) fileCache.AreaIDRange.first = area.ID;
			else if (area.ID > fileCache.AreaIDRange.second) fileCache.AreaIDRange.second = area.ID;
			index++;
			return true;
		}
		else {
			#ifndef NDEBUG
			std::clog << "NavFile::ReadData(index "<<std::to_string(index)<<"): failed to read area data.\n";
			#endif
			return false;
		}
	}))
	{
		return false;
	}

	// Read ladder count.
	if (buf.sgetn(reinterpret_cast<char*>(&LadderCount), VALVE_INT_SIZE) != VALVE_INT_SIZE) {
		#ifndef NDEBUG
		std::clog << "NavFile::ReadData(): Could not read ladder count.\n";
		#endif
		return false;
	}
	// Store ladder data.
	ladders.clear();
	ladders.resize(LadderCount);
	for (auto& ladder : ladders)
	{
		if (!ladder.ReadData(buf)) {
			#ifndef NDEBUG
			std::clog << "NavFile::ReadData(): Could not read ladder data.\n";
			#endif
			return false;
		}
		if (cache.LadderIDRange.first > ladder.ID) cache.LadderIDRange.first = ladder.ID;
		else if (cache.LadderIDRange.second < ladder.ID) cache.LadderIDRange.second = ladder.ID;
	}
	
	// Assume that remaining data left is custom data.
	// Done
	return true;
}

void NavFile::OutputData(std::ostream& ostream) {
  std::cout << "magic_number: " << MagicNumber
	<< "\nversion: " << Version;
  if (SubVersion.has_value()) ostream << "\nsubversion: " << SubVersion.value();
	if (BSPSize.has_value()) ostream << "\nmap_byte_size: " << BSPSize.value();
	if (isAnalyzed.has_value()) ostream << "\nanalyzed: " << std::boolalpha << isAnalyzed.value();
	ostream << "\nplace_count: " << PlaceCount;
	if (hasUnnamedAreas.has_value()) ostream << "\nunnamed_areas: " << std::boolalpha << hasUnnamedAreas.value();
	ostream << "\narea_count: " << AreaCount
	<< "\nladder_count: " << LadderCount;
}
