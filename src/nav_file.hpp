#ifndef NAV_FILE_HPP
#define NAV_FILE_HPP
#include <fstream>
#include <string>
#include <deque>
#include <filesystem>
#include <span>
#include "nav_base.hpp"
#include "nav_place.hpp"
#include "nav_area.hpp"

// Cache info for area.
struct NavFileCache {
  std::pair<unsigned int, unsigned int> AreaIDRange = {0u, 0u}, /* Store the range of possible area IDs */
    LadderIDRange = {0u, 0u}; // Store range of possible ladder IDs.
};

class NavFile {
	public:
		NavFileCache cache;
		// Header info
		unsigned int MagicNumber /* The magic number of the NAV file */, Version /* Version of the NAV file */;
		std::optional<unsigned int> SubVersion /* The subversion of the NAV file, included after version 10. */, BSPSize /* BSPSize of the NAV's map, included after version 4 */;
		std::optional<bool> isAnalyzed; // Introducted in major version 14.
		unsigned short PlaceCount; // Amount of places.
		std::optional<bool> hasUnnamedAreas; // Doesn't exist prior to version 14.
		unsigned int AreaCount = 0u, LadderCount = 0u;

		std::filesystem::path FilePath; // File path to NAV file.

		std::streampos AreaDataLoc = -1; // The starting location of area data.
		std::streampos LadderDataLoc = -1; // Ladder Data location.
		std::deque<std::string> PlaceNames; // Place Names
		std::vector<NavArea> areas; // Area container.
		std::deque<NavLadder> ladders; // Ladder container.

		/* Write to NAV file as stream.
		   Returns true on success, false on failure. */
		bool WriteData(std::streambuf& buf);
		/* Read from NAV file as stream.
		   Writes data to areaContainer.
		   Returns true on success, false on failure. */
		bool ReadData(std::streambuf& buf);
		// Output data.
		void OutputData(std::ostream& ostream);

		// Find an area with ID.
		// Retunrs the stream position if successful.
		std::optional<std::streampos> FindArea(const unsigned int& ID);
};
#endif
